<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    protected $fillable = ['user_id', 'service_name', 'cost_per_hour'];
    public $timestamps = false;

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'services';

    public function user() {
        return $this->belongsTo('App\User');
    }
}
