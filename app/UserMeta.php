<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{

    protected $fillable = ['user_id', 'company_name', 'company_email', 'company_website', 'phone_number'];

    public $timestamps = false;

    public function user() {
        return $this->belongsTo('App\User');
    }
}
