<?php

namespace App\Http\Controllers;

use App\UserMeta as UserMeta;
use Illuminate\Http\Request;
use App\Http\Resources\UserMetaResource as UserMetaResource;
use App\User as User;
use Auth;

class UserMetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserMeta  $userMeta
     * @return UserMetaResource
     */
    public function show(UserMeta $userMeta)
    {
        return new UserMetaResource($userMeta);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserMeta  $userMeta
     * @return \Illuminate\Http\Response
     */
    public function edit(UserMeta $userMeta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserMeta  $userMeta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserMeta $userMeta)
    {
        $data = $request->input();

        $meta = UserMeta::find($data['id']);

        $meta->company_name = $data['company_name'];
        $meta->company_email = $data['company_email'];
        $meta->company_website = $data['company_website'];
        $meta->phone_number = $data['phone_number'];

        $meta->save();

        return [
            'current' => $meta
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserMeta  $userMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserMeta $userMeta)
    {
        //
    }

    public function getAuthedUserMeta($user_id) {
        $meta = User::find($user_id)->with('userMeta');
        
        return [
            'meta' => UserMeta::where('user_id', $user_id)->get(),
            'user' => User::find($user_id)
        ];
    }

}
