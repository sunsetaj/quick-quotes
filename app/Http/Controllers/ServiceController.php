<?php

namespace App\Http\Controllers;

use App\Service as Service;
use Auth;
use Illuminate\Http\Request;
use App\User as User;

class ServiceController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'service_name' => 'required|max:255',
            'cost_per_hour' => 'required'
        ]);

        $data = $request->input();

        $service = new Service;

        $service->user_id = $data['user_id'];
        $service->service_name = $data['service_name'];
        $service->cost_per_hour = $data['cost_per_hour'];

        $service->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $request->validate([
            'service_name' => 'required|max:255',
            'cost_per_hour' => 'required',
            'id' => 'required'
        ]);

        $data = $request->input();

        Service::where('id', $data['id'])->update($data);
        $service = Service::find($data['id']);

        return [
            'updated_service' => $service
        ];
    }

    public function getAuthUserServices($user_id) {
        return [
            'services' => Service::where('user_id', $user_id)->get()
        ];
    }

    /**
     * Deletes a service entry in storage
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteService($service_id) {
        Service::where('id', $service_id)->delete();
    }
}
