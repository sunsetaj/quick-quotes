<?php
    $route = Route::current()->getName();
?>

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="qq">
            <ul id="navigation">
                @auth
                    <div class="profile">
                        <div class="profile-image"></div>
                        <span class="dropdown-trigger">{{ Auth::user()->name }}
                            <ul class="dropdown">
                                <li class="{{ $route == 'profile' ? 'active' : '' }}"><a href="{{ route('profile') }}">Profile</a></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        Sign Out
                                    </a>
            
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </span>
                    </div>
                    <li class="{{ $route == 'dashboard' ? 'active' : '' }}"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="{{ $route == 'new-quote' ? 'active' : '' }}"><a href="{{ route('new-quote') }}">New Quote</a></li>
                    <li class="{{ $route == 'clients' ? 'active' : '' }}"><a href="{{ route('clients') }}">Clients</a></li>
                    <li class="{{ $route == 'services' ? 'active' : '' }}"><a href="{{ route('services') }}">Services</a></li>
                @else
                    <li class="{{ $route == 'index' ? 'active' : '' }}"><a href="{{ route('index') }}">Home</a></li>
                    <li class="{{ $route == 'login' ? 'active' : '' }}"><a href="{{ route('login') }}">Login</a></li>
                    <li class="{{ $route == 'register' ? 'active' : '' }}"><a href="{{ route('register') }}">Register</a></li>
                @endauth
            </ul>
            <div id="app">
                @yield('content')
                <div id="react-render"></div>
            </div>
        </div>

        <!-- Scripts -->
        @include ('footer')
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
