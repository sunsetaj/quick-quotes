import React from 'react'

export const Input = (props) => {

    let label = props.input.name.split('_').join(' ');

    const changeValue = (e) => {
        props.onChangeEvent(e)
    }

    if (props.input.type != 'hidden') {
        return (
            <div className={`form-control ${ typeof props.input.control_class != 'undefined' ? props.input.control_class : '' }`}>
                <label>{label}</label>
                <input 
                    type={props.input.type} 
                    name={props.input.name} 
                    id={props.input.name} 
                    value={props.input.value} 
                    onChange={(e) => changeValue(e)}
                />
            </div>
        )
    }
    else {
        return (
            <input 
                type={props.input.type} 
                name={props.input.name}
                value={props.input.value}
            />
        )
    }
}

export const SubmitGroup = (props) => {
    return (
        <div className="form-control form-actions col-100">
            <button 
                type="submit"
                className={`btn ${ typeof props.additionalClasses != 'undefined' ? String(props.additionalClasses) : '' }`}
                onClick={ (e) => (props.action(e)) }
            >
            {props.title}
            </button>
        </div>
    )
}

export const MultipleInputs = (props) => {
    
    const inputOnChange = (e) => {
        props.onChangeFunc(e)
    }

    return props.inputs.map((input, index) => (
        <Input 
            key={index} 
            input={input} 
            onChangeEvent={(e) => inputOnChange(e)} 
        />
    ))
}