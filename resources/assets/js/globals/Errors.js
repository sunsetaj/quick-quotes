import React from 'react';

export const Errors = (props) => {
    let errors = props.errors;

    return (
        <div className="error-container">
            { Object.keys(errors).map((error, index) => {
                if (errors[error].length > 0) {
                    return (
                        errors[error].map((msg, index) => {
                            return (
                                <div key={index} className="alert" role="alert">
                                    { msg }
                                </div>
                            )
                        })
                    )
                }
            })}
        </div>
    );
};