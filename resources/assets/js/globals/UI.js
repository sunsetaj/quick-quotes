import React from 'react'

/**
 * Render <h1> with title
 * 
 * @param {string} props 
 */
export const PageTitle = (props) => (
    <div className="page-title">
        <h1>{props.title}</h1>
    </div>
)

export const Loading = (props) => {
    return (
        <div className="loading">
            <svg width="35px" height="35px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                <path stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" transform="rotate(150 50 51)">
                    <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform>
                </path>
            </svg>
            <p>Loading</p>
        </div>
    )
}