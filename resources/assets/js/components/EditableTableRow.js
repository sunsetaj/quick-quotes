import React, { Component, Fragment } from 'react'
import axios from 'axios'
import * as _ from 'lodash'

const no_display_fields = ['id', 'user_id']

export default class EditableTableRow extends Component {
    constructor() {
        super()
        this.state = {
            editable: false,
            row_data: null,
            edited_data: null
        }

        this.toggleEditableState = this.toggleEditableState.bind(this)
        this.onChange = this.onChange.bind(this)
        this.deleteRowInformation = this.deleteRowInformation.bind(this)
        this.updateRowInformation = this.updateRowInformation.bind(this)
        this.constructTableData = this.constructTableData.bind(this)
    }

    /**
     * Sets initial state on component Mount
     */
    componentDidMount() {
        this.setState({
            row_data: this.props.row_data,
            edited_data: this.props.row_data
        })
    }

    /**
     * Toggles `this.state.editable` to the opposite value
     * 
     * @param {object} e Anchor click event
     */
    toggleEditableState(e) {
        e.preventDefault();
        this.setState({
            editable: !this.state.editable,
            edited_data: this.state.row_data
        })
    }

    /**
     * Saves the edited values in state
     * 
     * @param {object} e Anchor click event object
     */
    onChange(e) {
        let copyState = {...this.state.edited_data}
        copyState[e.target.name] = e.target.value

        this.setState({
            edited_data: copyState
        })
    }

    /**
     * Deletes a record from the DB, url needs to be passed through prop `deleteAPIPrefix`
     * 
     * @param {Object} e Anchor click event object
     * @param {string} _id object id
     */
    deleteRowInformation(e, _id) {
        e.preventDefault()
        const _this = this;

        axios({
            method: 'delete',
            url: _this.props.deleteAPIPrefix + _id,
            headers: {
                'X-CSRF-TOKEN': document.querySelector("meta[name='csrf-token']").getAttribute('content')
            }
        })
        .then((response) => {
            _this.props.refetch();
        })
        .catch((err) => {
            console.error(err)
        })
    }

    /**
     * Updates a record from the DB, url is passed through the `updateAPIPrefix` prop
     * 
     * @param {Object} e Anchor click event object
     */
    updateRowInformation(e) {
        e.preventDefault()
        const _this = this

        axios({
            method: 'post',
            url: _this.props.updateAPIPrefix,
            data: _this.state.edited_data,
            headers: {
                'X-CSRF-TOKEN': document.querySelector("meta[name='csrf-token']").getAttribute('content')
            }
        })
        .then((response) => {
            if (response.status === 200) {
                _this.setState({
                    row_data: _this.state.edited_data,
                    editable: false
                })
            }
        })
        .catch((err) => {
            console.error(err)
        })
    }

    /**
     * Generate the table row based on data passed to Component.
     * If `this.state.editable` is true, it will render an input inside the `<td>`
     */
    constructTableData() {
        const data = this.state.row_data;

        let rows = Object.keys(data).map((key, index) => {
            if (_.indexOf(no_display_fields, key) === -1) {
                return this.state.editable ? (
                    <td key={index}>
                        <input name={key} value={this.state.edited_data[key]} onChange={ (e) => ( this.onChange(e) ) } />
                    </td>
                ) : (
                    <td key={index}>{data[key]}</td>
                )
            }
        })

        return rows;
    }

    render() {
        let row_data = this.state.row_data;

        return row_data !== null ? (
            <tr>
                { this.constructTableData() }
                <td className="td-actions">
                    {
                        this.state.editable ? (
                            <Fragment>
                                <a href="#" className="btn btn-primary" onClick={ (e) => ( this.updateRowInformation(e) ) }>Save</a>
                                <a href="#" className="btn" onClick={ (e) => ( this.toggleEditableState(e) ) }>Cancel</a>
                            </Fragment>
                        ) : (
                            <Fragment>
                                <a href="#" className="btn" onClick={ (e) => ( this.toggleEditableState(e) ) }>Edit</a>
                                <a href="#" className="btn" onClick={ (e) => ( this.deleteRowInformation(e, row_data.id) ) }>Delete</a>
                            </Fragment>
                        )
                    }
                </td>
            </tr>
        ) : (<tr></tr>)
    }

}