import React from 'react'

export const LabelValue = (props) => {
    if (props.value !== null && props.value !== '') {

        let url = props.value.search(/(^http:|^https:)/g);
        let value = props.value;

        if (url === 0) {
            value = <a rel="noopener noreferrer" target="_blank" href={`${props.value}`}>{props.value}</a>
        }

        return (
            <div className="label-value-pair">
                <span className="label">{props.label}</span>
                <span className="value">{value}</span>
            </div>
        )
    }
    return false
}