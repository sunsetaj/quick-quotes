import React, { Component, Fragment } from 'react'

import LoginForm from '../webforms/LoginForm'

export default class Login extends Component {
    constructor() {
        super()
    }

    render() {
        return (
            <Fragment>
                <div className="content">
                    <LoginForm />
                </div>
            </Fragment>
        )
    }
}