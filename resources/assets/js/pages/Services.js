import React, { Component, Fragment } from 'react'
import axios from 'axios'

import ServiceForm from '../webforms/ServiceForm'
import EditableTableRow from '../components/EditableTableRow'

import Card, { Card_Header, Card_Body } from '../components/Card'
import Modal from '../components/Modal'
import { Loading } from '../globals/UI'
import { LabelValue } from '../components/LabelValue'
import { EditIcon } from '../globals/Icons'

export default class Services extends Component {
    constructor() {
        super()

        this.state = {
            detectSubmit: false,
            service_information: null
        }

        this.changeSubmitState = this.changeSubmitState.bind(this)
        this.constructServiceInfo = this.constructServiceInfo.bind(this)
        this.fetchServiceData = this.fetchServiceData.bind(this)
    }

    fetchServiceData() {
        const _this = this;

        this.setState({
            service_information: null
        })

        axios({
            method: 'get',
            url: '/api/services/' + sessionStorage.getItem('user_id'),
        })
        .then((response) => {
            _this.setState({
                service_information: response.data.services,
            })
        })
        .catch((err) => {
            console.error(err);
        })
    }

    componentDidMount() {
        this.fetchServiceData();
    }

    changeSubmitState(state) {
        this.setState({
            detectSubmit: state
        })
    }

    constructServiceInfo() {

        let service_action = [
            {
                title: 'Save',
                classnames: 'btn btn-primary',
                action: () => ( this.changeSubmitState(true) )
            }
        ]

         return this.state.service_information.map((service) => {
            return (
                <EditableTableRow 
                    key={service.id}
                    row_data={service}
                    refetch={() => (this.fetchServiceData())}
                    deleteAPIPrefix="/api/service/"
                    updateAPIPrefix="/api/service/update"
                />
            )
        })
    }

    render() {

        let services_action = [
            {
                title: 'Save',
                classnames: 'btn btn-primary',
                action: () => ( this.changeSubmitState(true) )
            }
        ]

        return (
            <Card template="full">
                <Card_Header title="Services">
                    <div className="card-actions text-align--right">
                        <Modal title="Add New Service" buttonText="Add" actions={services_action} openModel={ this.state.editFormState }>
                            <Fragment>
                                <ServiceForm 
                                    detectSubmit={this.state.detectSubmit}
                                    changeSubmitState={(val) => (this.changeSubmitState(val))}
                                    refetch={() => (this.fetchServiceData())}
                                />
                            </Fragment>
                        </Modal>
                    </div>
                </Card_Header>
                <Card_Body>
                    { this.state.service_information === null ? ( <Loading /> ) :  (
                        <Fragment>
                            { this.state.service_information.length === 0 ? (
                                <div className="loading"><p>No Data</p></div>
                            ) : (
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Service Name</th>
                                            <th>Cost Per Hour ($)</th>
                                            <th className="actions">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { this.constructServiceInfo() }
                                    </tbody>
                                </table>
                            ) }
                        </Fragment>
                    )}
                </Card_Body>
            </Card>
        )
    }
}