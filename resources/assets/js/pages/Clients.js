import React, { Component } from 'react'

import Card from '../components/Card'

export default class Clients extends Component {
    render() {
        return (
            <Card title="Clients">
                List of all Clients
            </Card>
        )
    }
}