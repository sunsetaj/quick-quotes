import React, { Component } from 'react'
import axios from 'axios'

import { PageTitle } from '../globals/UI'
import Card, { Card_Header, Card_Body } from '../components/Card'
import Modal from '../components/Modal'
import { Loading } from '../globals/UI'
import { LabelValue } from '../components/LabelValue'
import UpdateUserMeta from '../webforms/UpdateUserMeta'

export default class Profile extends Component {
    constructor() {
        super()
        this.state = {
            user: null,
            user_meta: null,
            submit: false
        }

        this.constructUserInfo = this.constructUserInfo.bind(this)
        this.detectSubmit = this.detectSubmit.bind(this)
        this.setTempState = this.setTempState.bind(this)
    }

    componentDidMount() {
        const _this = this;

        axios({
            method: 'get',
            url: '/api/meta/' + sessionStorage.getItem('user_id'),
        })
        .then((response) => {
            _this.setState({
                user: response.data.user,
                user_meta: response.data.meta[0]
            })
        })
        .catch((err) => {
            console.error(err);
        })
    }

    constructUserInfo() {
        let user = this.state.user,
            user_meta = this.state.user_meta;
        
        let user_info = [
            {
                label: 'Company Name',
                value: user_meta.company_name
            },
            {
                label: 'Company Email',
                value: user_meta.company_email
            },
            {
                label: 'Company Website',
                value: user_meta.company_website
            },
            {
                label: 'Phone Number',
                value: user_meta.phone_number
            }
        ]

        return user_info.map((info, index) => (
            <LabelValue value={info.value} label={info.label} key={index} />
        ))
    }

    detectSubmit(val) {
        this.setState({
            'submit': val
        })
    }

    setTempState(tempState) {
        this.setState({
            user_meta: {...tempState.current}
        });
    }

    render() {
        let no_meta_data = true,
            user_meta = this.state.user_meta;

        if (user_meta !== null) {
            if (user_meta.company_email || user_meta.company_name || user_meta.company_website || user_meta.phone_number) {
                no_meta_data = false;
            }
        }

        let modal_actions = [
            {
                title: 'Update',
                classnames: 'btn btn-primary',
                action: () => { this.detectSubmit(true) }
            }
        ]

        return (
            <Card template="full">
                <Card_Header title="Profile">
                    <div className="card-actions text-align--right">
                        <Modal title="Update User Meta" buttonText="Edit" actions={modal_actions}>
                            <div>
                                <UpdateUserMeta 
                                    currentMeta={user_meta} 
                                    detectSubmit={this.state.submit} 
                                    removeSubmitDetect={ (val) => (this.detectSubmit(val)) }
                                    setTempState={ (tempState) => (this.setTempState(tempState)) }
                                />
                            </div>
                        </Modal>
                    </div>
                </Card_Header>
                <Card_Body>
                {
                    this.state.user === null ? (
                        <Loading />
                    ) : (
                        no_meta_data === true ? (
                            <p>No Current data</p>
                        ) : (
                            this.constructUserInfo()
                        )
                    )
                }
                </Card_Body>
            </Card>
        )
    }
}