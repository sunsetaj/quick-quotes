import React, { Component, Fragment } from 'react'

import RegisterForm from '../webforms/RegisterForm'

export default class Register extends Component {
    constructor() {
        super()
    }

    render() {
        return (
            <Fragment>
                <div className="content">
                    <RegisterForm />
                </div>
            </Fragment>
        )
    }
}