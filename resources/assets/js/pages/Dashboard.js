import React, { Component } from 'react'

import { PageTitle } from '../globals/UI'
import Card from '../components/Card'

export default class Dashboard extends Component {
    render() {
        return (
            <Card title="Dashboard">
                List of all Quotes
            </Card>
        )
    }
}