import React, { Fragment } from 'react'
import ReactDOM from 'react-dom'
import {
    BrowserRouter as Router,
    Link,
    Switch,
    Route
} from 'react-router-dom'

// Pages
import NotFound from './pages/NotFound'
import Login from './pages/Login'
import Register from './pages/Register'
import Quotes from './pages/Quotes'
import Dashboard from './pages/Dashboard'
import Profile from './pages/Profile'
import Services from './pages/Services'
import Clients from './pages/Clients'

class App extends React.Component {
    render() {
        if (window.user_id !== 'undefined') {
            sessionStorage.setItem('user_id', window.user_id);
        }
        else {
            sessionStorage.removeItem('user_id');
        }

        return (
            <Router>
                <Fragment>
                    <div className="content-container">
                        <Switch>
                            <Route path="/login" component={Login} />
                            <Route path="/register" component={Register} />
                            <Route path="/dashboard" component={Dashboard} />
                            <Route path="/quotes" component={Quotes} />
                            <Route path="/profile" component={Profile} />
                            <Route path="/services" component={Services} />
                            <Route path="/clients" component={Clients} />
                        </Switch>
                    </div>
                </Fragment>
            </Router>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('react-render'));