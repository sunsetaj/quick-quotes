import React, { Component, Fragment } from 'react'
import axios from 'axios'

import { MultipleInputs, SubmitGroup } from '../form/Inputs'
import Card, { Card_Header, Card_Footer, Card_Body } from '../components/Card'
import { Errors } from '../globals/Errors'

export default class LoginForm extends Component {
    constructor() {
        super()

        this.state = {
            email: '',
            password: '',
            errors: {
                email: [],
                password: []
            }
        }

        this.authenticateUser = this.authenticateUser.bind(this)
        this.onInputChange = this.onInputChange.bind(this)
        this.renderFormInputs = this.renderFormInputs.bind(this)
    }

    authenticateUser(event) {
        const _this = this;
        event.preventDefault()
        
        axios({
            method: 'post',
            'url': '/login',
            data: {
                email: _this.state.email,
                password: _this.state.password
            }, 
            headers: {
                'X-CSRF-TOKEN': document.querySelector("meta[name='csrf-token']").getAttribute('content')
            }
        })
        .then((response) => {
            if (response.status === 200) {
                window.location = 'dashboard';
            }
        })
        .catch((error) => {
            _this.setState({
                errors: Object.assign(
                    {},
                    {
                        email: [],
                        password: []
                    },
                    error.response.data.errors
                )
            })
        })
    }

    onInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    renderFormInputs() {
        let formInputs = [
            {
                'name': 'email',
                'value': this.state.email,
                'type': 'text',
                'control_class': `form-inline ${ this.state.errors.email.length > 0 ? 'error' : '' }`
            },
            {
                'name': 'password',
                'value': this.state.password,
                'type': 'password',
                'control_class': `form-inline ${ this.state.errors.password.length > 0 ? 'error' : '' }`
            },
            {
                'name': '_token',
                'value': document.querySelector("meta[name='csrf-token']").getAttribute('content'),
                'type': 'hidden'
            }
        ]
        return <MultipleInputs inputs={formInputs} onChangeFunc={(e) => (this.onInputChange(e))} />
    }

    render() {
        let errors = this.state.errors
        return (
            <Fragment>
                <Card template="full">
                    <Card_Header title="Login" />
                    <Card_Body>
                        { errors.email.length > 0 || errors.password.length > 0 ? <Errors errors={errors} /> : '' }
                        <form>
                            { this.renderFormInputs() }
                        </form>
                    </Card_Body>
                </Card>
                <SubmitGroup title="Login" additionalClasses="btn-primary" action={(event) => (this.authenticateUser(event))} />
            </Fragment>
        )
    }
}