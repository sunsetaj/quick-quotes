import React, { Component, Fragment } from 'react'
import axios from 'axios'

import { MultipleInputs, SubmitGroup } from '../form/Inputs'
import { Errors } from '../globals/Errors'

export default class ServiceForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            service_name: typeof this.props.service_name !== 'undefined' ? this.props.service_name : '',
            cost_per_hour: typeof this.props.cost_per_hour !== 'undefined' ? this.props.cost_per_hour : '',
            post_id: typeof this.props.post_id !== 'undefined' ? this.props.post_id : null,
            errors: {
                service_name: [],
                cost_per_hour: []
            }
        }

        this.renderInputs = this.renderInputs.bind(this)
        this.onInputChange = this.onInputChange.bind(this)
        this.saveService = this.saveService.bind(this)
    }

    componentDidUpdate() {
        if (this.props.detectSubmit === true) {
            this.props.changeSubmitState(false);
            this.saveService();
        }
    }

    saveService() {

        const _this = this;
        let url = '/api/service/create'

        let data = {
            service_name: this.state.service_name,
            cost_per_hour: this.state.cost_per_hour,
            user_id: window.sessionStorage.getItem('user_id')
        }

        axios({
            method: 'post',
            url: url,
            data: data,
            headers: {
                'X-CSRF-TOKEN': document.querySelector("meta[name='csrf-token']").getAttribute('content')
            }
        })
        .then((response) => {
            if (response.status == 200) {
                this.props.refetch();
                _this.setState({
                    service_name: '',
                    cost_per_hour: '',
                    errors: {
                        service_name: [],
                        cost_per_hour: []
                    }
                });
            }
        })
        .catch((err) => {
            _this.setState({
                errors: Object.assign(
                    {},
                    {
                        service_name: [],
                        cost_per_hour: []
                    },
                    err.response.data.errors
                )
            })
        })
    }

    onInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    renderInputs() {
        let inputs = [
            {
                name: 'service_name',
                value: this.state.service_name,
                type: 'text',
                control_class: `form-inline ${ this.state.errors.service_name.length > 0 ? 'error' : '' }`
            },
            {
                name: 'cost_per_hour',
                value: this.state.cost_per_hour,
                type: 'number',
                control_class: `form-inline ${ this.state.errors.cost_per_hour.length > 0 ? 'error' : '' }`
            }
        ]

        return <MultipleInputs inputs={inputs} onChangeFunc={(e) => (this.onInputChange(e))} />
    }

    render() {
        let errors = this.state.errors
        return (
            <Fragment>
                { errors.service_name.length > 0 || errors.cost_per_hour.length > 0 ? <Errors errors={errors} /> : '' }
                <form>
                    { this.renderInputs() }
                </form>
            </Fragment>
        )
    }
}