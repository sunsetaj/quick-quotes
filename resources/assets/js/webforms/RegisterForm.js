import React, { Component, Fragment } from 'react'
import axios from 'axios'

import { MultipleInputs, SubmitGroup } from '../form/Inputs'
import Card, { Card_Header, Card_Footer, Card_Body } from '../components/Card'
import { Errors } from '../globals/Errors'

export default class RegisterForm extends Component {
    constructor() {
        super()

        this.state = {
            name: '',
            email: '',
            password: '',
            password_confirmation: '',
            errors: {
                email: [],
                name: [],
                password: []
            }
        }

        this.registerUser = this.registerUser.bind(this)
        this.onInputChange = this.onInputChange.bind(this)
        this.renderFormInputs = this.renderFormInputs.bind(this)
    }

    registerUser(e) {
        const _this = this
        e.preventDefault()
        axios({
            method: 'post',
            url: '/register',
            data: {
                name: _this.state.name,
                email: _this.state.email,
                password: _this.state.password,
                password_confirmation: _this.state.password_confirmation
            },
            headers: {
                'X-CSRF-TOKEN': document.querySelector("meta[name='csrf-token']").getAttribute('content')
            }
        })
        .then((response) => {
            console.log(response);
            if (response.status === 200) {
                window.location = 'dashboard';
            }
        })
        .catch((error) => {
            console.error(error);
            _this.setState({
                errors: Object.assign(
                    {},
                    {
                        email: [],
                        name: [],
                        password: []
                    },
                    error.response.data.errors
                )
            })
        })
    }

    onInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    renderFormInputs() {
        let formInputs = [
            {
                'name': 'name',
                'value': this.state.name,
                'type': 'text',
                'control_class': `form-inline ${ this.state.errors.name.length > 0 ? 'error' : '' }`
            },
            {
                'name': 'email',
                'value': this.state.email,
                'type': 'text',
                'control_class': `form-inline ${ this.state.errors.email.length > 0 ? 'error' : '' }`
            },
            {
                'name': 'password',
                'value': this.state.password,
                'type': 'password',
                'control_class': `form-inline ${ this.state.errors.password.length > 0 ? 'error' : '' }`
            },
            {
                'name': 'password_confirmation',
                'value': this.state.password_confirmation,
                'type': 'password',
                'control_class': `form-inline ${ this.state.errors.password.length > 0 ? 'error' : '' }`
            },
            {
                'name': '_token',
                'value': document.querySelector("meta[name='csrf-token']").getAttribute('content'),
                'type': 'hidden'
            }
        ]
        return <MultipleInputs inputs={formInputs} onChangeFunc={(e) => (this.onInputChange(e))} />
    }

    render() {
        let errors = this.state.errors
        return (
            <Fragment>
                <Card template="full">
                    <Card_Header title="Register" />
                    <Card_Body>
                        { errors.name.length > 0 || errors.email.length > 0 || errors.password.length > 0 ? <Errors errors={errors} /> : '' }
                        <form id="register-form">
                            { this.renderFormInputs() }
                        </form>
                    </Card_Body>
                </Card>
                <SubmitGroup title="Register" additionalClasses="btn-primary" action={ (e) => (this.registerUser(e)) } />
            </Fragment>
        )
    }
}