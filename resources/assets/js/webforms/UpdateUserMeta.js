import React, { Component } from 'react'
import axios from 'axios'

import { MultipleInputs } from '../form/Inputs'

export default class UpdateUserMeta extends Component {
    constructor(props) {
        super(props)

        this.state = {
            company_email: '',
            company_name: '',
            company_website: '',
            phone_number: ''
        }

        this.onInputChange = this.onInputChange.bind(this)
        this.renderFormInputs = this.renderFormInputs.bind(this)
        this.updateUserMeta = this.updateUserMeta.bind(this)
    }

    componentDidUpdate() {
        if (this.props.detectSubmit === true) {
            this.props.removeSubmitDetect(false)
            this.updateUserMeta()
        }
    }

    componentWillReceiveProps(nextProps) {
        let meta = this.props.currentMeta
        let nextMeta = nextProps.currentMeta
        let newMeta = {}

        if (meta === null && nextMeta !== null) {
            newMeta = {...nextMeta}

            for (let key in newMeta) {
                if (newMeta[key] === null) {
                    newMeta[key] = '';
                }
            }

            this.setState(
                {...newMeta}
            );
        }
    }

    onInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    updateUserMeta() {
        const _this = this;
        let data = {...this.state}
        axios({
            method: 'post',
            url: '/api/meta/update',
            data: data,
            headers: {
                'X-CSRF-TOKEN': document.querySelector("meta[name='csrf-token']").getAttribute('content')
            }
        })
        .then((response) => {
            _this.props.setTempState(response.data)
        })
        .catch((err) => {
            console.error(err);
        })
    }

    renderFormInputs() {
        let formInputs = [
            {
                'name': 'company_name',
                'value': this.state.company_name,
                'type': 'text',
                'control_class': 'form-inline'
            },
            {
                'name': 'company_email',
                'value': this.state.company_email,
                'type': 'email',
                'control_class': 'form-inline'
            },
            {
                'name': 'company_website',
                'value': this.state.company_website,
                'type': 'text',
                'control_class': 'form-inline'
            },
            {
                'name': 'phone_number',
                'value': this.state.phone_number,
                'type': 'text',
                'control_class': 'form-inline'
            }
        ]

        return <MultipleInputs inputs={formInputs} onChangeFunc={(e) => (this.onInputChange(e))} />
    }

    render() {
        return (
            <form>
                { this.props.currentMeta !== null ? this.renderFormInputs() : '' }
            </form>
        )
    }
}