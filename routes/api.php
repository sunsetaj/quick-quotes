<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::resource('usermeta', 'UserMetaController');

Route::get('/meta/{user_id}', 'UserMetaController@getAuthedUserMeta');
Route::post('/meta/update', 'UserMetaController@update');

Route::post('/service/create', 'ServiceController@store');
Route::post('/service/update', 'ServiceController@update');
Route::get('/services/{user_id}', 'ServiceController@getAuthUserServices');
Route::delete('/service/{service_id}', 'ServiceController@deleteService');