<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/quote/new', 'HomeController@index')->name('new-quote');
Route::get('/clients', 'HomeController@index')->name('clients');
Route::get('/quotes', 'HomeController@index')->name('all-quotes');
Route::get('/profile', 'HomeController@index')->name('profile');
Route::get('/services', 'HomeController@index')->name('services');
